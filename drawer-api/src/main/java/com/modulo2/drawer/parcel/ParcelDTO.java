package com.modulo2.drawer.parcel;

import java.time.LocalDateTime;

public record ParcelDTO(Long id, String receiverEmail, String senderEmail, LocalDateTime expirationDate,
                        Boolean isExpired) {
}
