package com.modulo2.drawer.parcel;

import com.modulo2.drawer.parcel.mapper.ParcelMapper;
import com.modulo2.drawer.parcel.reqrsp.SendParcelReq;
import com.modulo2.drawer.util.Secretor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ParcelService {

    private final ParcelRepository parcelRepository;
    private final ParcelMapper parcelMapper;

    @Autowired
    public ParcelService(ParcelRepository parcelRepository, ParcelMapper parcelMapper) {
        this.parcelRepository = parcelRepository;
        this.parcelMapper = parcelMapper;
    }

    public ParcelDTO save(SendParcelReq parcelReq) {
        final String token = UUID.randomUUID().toString();
        String encSec;
        try {
            encSec = Secretor.encrypt(token, parcelReq.getSecretParcel());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        final Parcel parcel = new Parcel();
        parcel.setHashedSecret(encSec);
        parcel.setExpirationDate(parcelReq.getTimeToExplode());
        parcel.setSenderEmail(parcel.getSenderEmail());
        parcel.setReceiverEmail(parcel.getReceiverEmail());
        parcelRepository.save(parcel);
        return parcelMapper.mapParcelToParcelDto(parcel);
    }
}
