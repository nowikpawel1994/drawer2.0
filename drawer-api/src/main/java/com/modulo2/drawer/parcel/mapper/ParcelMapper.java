package com.modulo2.drawer.parcel.mapper;

import com.modulo2.drawer.parcel.Parcel;
import com.modulo2.drawer.parcel.ParcelDTO;
import org.mapstruct.Mapper;

@Mapper
public interface ParcelMapper {

    ParcelDTO mapParcelToParcelDto(Parcel parcel);
}
