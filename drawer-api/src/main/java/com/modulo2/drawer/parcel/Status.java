package com.modulo2.drawer.parcel;

public enum Status {
    PARCEL_PROCESSING,
    PARCEL_RECEIVED,
    PARCEL_NO_RECEIVED;
}
