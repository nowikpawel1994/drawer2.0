package com.modulo2.drawer.parcel;

import jakarta.persistence.Entity;
import lombok.Data;

import java.time.LocalDateTime;

@Entity
@Data
public class Parcel {
    private String id;
    private String receiverEmail;
    private String senderEmail;
    private String hashedSecret;
    private LocalDateTime expirationDate;

}
