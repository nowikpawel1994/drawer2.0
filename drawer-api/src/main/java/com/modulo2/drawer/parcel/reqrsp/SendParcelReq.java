package com.modulo2.drawer.parcel.reqrsp;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SendParcelReq {

    @NotNull
    @Email
    String senderEmail;

    @NotNull
    @Email
    String receiverEmail;

    @NotNull
    @NotEmpty

    String secretParcel;

    LocalDateTime timeToExplode;

}
